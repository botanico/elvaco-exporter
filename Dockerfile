FROM golang:1.18 AS compile
ADD . /root
WORKDIR /root
ENV CGO_ENABLED=0
RUN go build -o elvaco-exporter

FROM alpine:latest
COPY --from=compile /root/elvaco-exporter /usr/local/bin/
CMD /usr/local/bin/elvaco-exporter
