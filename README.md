# Elvaco Exporter

Configure elvaco:

```
telnet 192.168.3.82 9999
login 3333
get
set common.http.url=http://192.168.50.2:8090/post
```

See <https://support.elvaco.com/hc/en-us/articles/4420244072721-How-to-whitelist-content-in-a-CMe3100-CMe2100-pushreport->

Google for CMe2100 sms commands to find the commands.