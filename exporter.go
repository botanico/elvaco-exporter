package main

import (
	"encoding/csv"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"strconv"
	"strings"
	"unicode"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	Water                    *prometheus.GaugeVec
	HeatingEnergy            *prometheus.GaugeVec
	HeatingVolume            *prometheus.GaugeVec
	HeatingHoursActive       *prometheus.GaugeVec
	HeatingFlowTemperature   *prometheus.GaugeVec
	HeatingReturnTemperature *prometheus.GaugeVec
	HeatingDeltaTemperature  *prometheus.GaugeVec
	HeatingPower             *prometheus.GaugeVec
	HeatingPowerMax          *prometheus.GaugeVec
	HeatingVolumeFlow        *prometheus.GaugeVec
	HeatingVolumeFlowMax     *prometheus.GaugeVec
)

func init() {
	Water = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "elvaco_water_volume",
		Help: "Elvaco water volume reading (m3)",
	}, []string{"serial", "device_id", "manufacturer"})

	HeatingEnergy = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "elvaco_heating_energy",
		Help: "Elvaco heating/cooling load reading for energy (Wh)",
	}, []string{"serial", "device_id", "manufacturer"})

	HeatingVolume = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "elvaco_heating_volume",
		Help: "Elvaco heating/cooling load reading for volume (m3)",
	}, []string{"serial", "device_id", "manufacturer"})

	HeatingHoursActive = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "elvaco_heating_ontime",
		Help: "Elvaco heating/cooling load reading for on-time (hours)",
	}, []string{"serial", "device_id", "manufacturer"})

	HeatingFlowTemperature = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "elvaco_heating_flow_temperature",
		Help: "Elvaco heating/cooling load reading for flow temperature (Celcius)",
	}, []string{"serial", "device_id", "manufacturer"})

	HeatingReturnTemperature = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "elvaco_heating_return_temperature",
		Help: "Elvaco heating/cooling load reading for return temperature (Celcius)",
	}, []string{"serial", "device_id", "manufacturer"})

	HeatingDeltaTemperature = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "elvaco_heating_delta_temperature",
		Help: "Elvaco heating/cooling load reading for delta temperature (Celcius)",
	}, []string{"serial", "device_id", "manufacturer"})

	HeatingPower = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "elvaco_heating_power",
		Help: "Elvaco heating/cooling load reading for power (W)",
	}, []string{"serial", "device_id", "manufacturer"})

	HeatingPowerMax = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "elvaco_heating_power_max",
		Help: "Elvaco heating/cooling load reading for power (W, max)",
	}, []string{"serial", "device_id", "manufacturer"})

	HeatingVolumeFlow = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "elvaco_heating_volume_flow",
		Help: "Elvaco heating/cooling load reading for volume flow (m3/h)",
	}, []string{"serial", "device_id", "manufacturer"})

	HeatingVolumeFlowMax = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "elvaco_heating_volume_flow_max",
		Help: "Elvaco heating/cooling load reading for volume flow (m3/h, max)",
	}, []string{"serial", "device_id", "manufacturer"})

	prometheus.MustRegister(Water)
	prometheus.MustRegister(HeatingEnergy)
	prometheus.MustRegister(HeatingVolume)
	prometheus.MustRegister(HeatingHoursActive)
	prometheus.MustRegister(HeatingFlowTemperature)
	prometheus.MustRegister(HeatingReturnTemperature)
	prometheus.MustRegister(HeatingDeltaTemperature)
	prometheus.MustRegister(HeatingPower)
	prometheus.MustRegister(HeatingPowerMax)
	prometheus.MustRegister(HeatingVolumeFlow)
	prometheus.MustRegister(HeatingVolumeFlowMax)
}

func main() {
	http.HandleFunc("/post", post)
	http.Handle("/metrics", promhttp.Handler())

	log.Fatal(http.ListenAndServe(":8090", nil))
}

func post(w http.ResponseWriter, req *http.Request) {
	reqDump, err := httputil.DumpRequest(req, true)
	if err != nil {
		log.Printf("Could not parse request: %s", err)
		http.Error(w, "Could not parse request", http.StatusBadRequest)
		return
	}

	if req.Method != "POST" {
		log.Printf("Request is not POST:\n%s", reqDump)
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	if req.Header.Get("Content-Type") != "application/octet-stream" {
		log.Printf("Request has invalid content-type:\n%s", reqDump)
		http.Error(w, "Content-Type not allowed", http.StatusBadRequest)
		return
	}

	filename := req.Header.Get("Filename")
	if filename == "" {
		log.Printf("Request has no filename:\n%s", reqDump)
		http.Error(w, "Filename not found", http.StatusBadRequest)
		return
	}

	reader := csv.NewReader(req.Body)
	reader.Comma = ';'
	reader.Comment = 0

	data, err := reader.ReadAll()
	if err != nil {
		log.Printf("Failed to read body:\n%s", reqDump)
		http.Error(w, "Error reading body", http.StatusBadRequest)
		return
	}

	err = parse(data)
	if err != nil {
		log.Printf("Failed to parse body:\n%s", reqDump)
		http.Error(w, "Error parsing body", http.StatusBadRequest)
		return
	}

	http.Error(w, "OK", http.StatusOK)
}

func parse(data [][]string) error {
	// serial-number;device-identification;created;value-data-count;manufacturer;version;device-type;access-number;status;signature
	// volume,m3,inst-value,0,0,0;datetime,,inst-value,0,0,0;date,,inst-value,0,0,1;volume,m3,inst-value,0,0,1;date future-value,,inst-value,0,0,1;fabrication-no,,inst-value,0,0,0;date,,inst-value,0,0,2;volume,m3,inst-value,0,0,2;date,,inst-value,0,0,3;volume,m3,inst-value,0,0,3;date,,inst-value,0,0,4;volume,m3,inst-value,0,0,4;date,,inst-value,0,0,5;volume,m3,inst-value,0,0,5;date,,inst-value,0,0,6;volume,m3,inst-value,0,0,6;date,,inst-value,0,0,7;volume,m3,inst-value,0,0,7;date,,inst-value,0,0,8;volume,m3,inst-value,0,0,8;date,,inst-value,0,0,9;volume,m3,inst-value,0,0,9;date,,inst-value,0,0,10;volume,m3,inst-value,0,0,10;date,,inst-value,0,0,11;volume,m3,inst-value,0,0,11;date,,inst-value,0,0,12;volume,m3,inst-value,0,0,12;date,,inst-value,0,0,13;volume,m3,inst-value,0,0,13;manufacturer-specific,,inst-value,0,0,0
	// fabrication-no,,inst-value,0,0,0;energy,Wh,inst-value,0,0,0;energy manufacturer-specific-02,Wh,inst-value,0,0,0;manufacturer-specific-ff-07,,inst-value,0,0,0;manufacturer-specific-ff-08,,inst-value,0,0,0;volume,m3,inst-value,0,0,0;on-time,hour(s),inst-value,0,0,0;on-time,hour(s),err-value,0,0,0;flow-temp,�C,inst-value,0,0,0;return-temp,�C,inst-value,0,0,0;diff-temp,K,inst-value,0,0,0;power,W,inst-value,0,0,0;power,W,max-value,0,0,0;volume-flow,m3/h,inst-value,0,0,0;volume-flow,m3/h,max-value,0,0,0;error-flags-dev-spec,,inst-value,0,0,0;datetime,,inst-value,0,0,0;energy,Wh,inst-value,0,0,1;energy manufacturer-specific-02,Wh,inst-value,0,0,1;manufacturer-specific-ff-07,,inst-value,0,0,1;manufacturer-specific-ff-08,,inst-value,0,0,1;volume,m3,inst-value,0,0,1;power,W,max-value,0,0,1;volume-flow,m3/h,max-value,0,0,1;date,,inst-value,0,0,1;manufacturer-specific-ff-0f,,inst-value,0,0,0;manufacturer-specific-ff-11,,inst-value,0,0,0;manufacturer-specific-ff-12,,inst-value,0,0,0;manufacturer-specific-ff-1a,,inst-value,0,0,0;fw-version,,inst-value,0,0,0

	if len(data) < 2 {
		return errors.New("not enough rows")
	}

	header := data[0]

	for _, row := range data[1:] {
		m := map[string]string{}

		for i, col := range row {
			if i >= len(header) {
				break
			}

			m[removeNonAscii(header[i])] = col
		}

		if err := parseRow(m); err != nil {
			return err
		}
	}

	return nil
}

func parseRow(m map[string]string) error {
	vec := prometheus.Labels{
		"serial":       m["#serial-number"],
		"device_id":    m["device-identification"],
		"manufacturer": m["manufacturer"],
	}

	// gauge.With(vec).Set(avg)

	switch m["device-type"] {
	case "heat/cooling load":
		// fabrication-no,,inst-value,0,0,0;
		// energy,Wh,inst-value,0,0,0;
		HeatingEnergy.With(vec).Set(toFloat(m["energy,Wh,inst-value,0,0,0"]))
		// energy manufacturer-specific-02,Wh,inst-value,0,0,0;
		// manufacturer-specific-ff-07,,inst-value,0,0,0;
		// manufacturer-specific-ff-08,,inst-value,0,0,0;
		// volume,m3,inst-value,0,0,0;
		HeatingVolume.With(vec).Set(toFloat(m["volume,m3,inst-value,0,0,0"]))
		// on-time,hour(s),inst-value,0,0,0;
		HeatingHoursActive.With(vec).Set(toFloat(m["on-time,hour(s),inst-value,0,0,0"]))
		// on-time,hour(s),err-value,0,0,0;
		// flow-temp,�C,inst-value,0,0,0;
		HeatingFlowTemperature.With(vec).Set(toFloat(m["flow-temp,C,inst-value,0,0,0"]))
		// return-temp,�C,inst-value,0,0,0;
		HeatingReturnTemperature.With(vec).Set(toFloat(m["return-temp,C,inst-value,0,0,0"]))
		// diff-temp,K,inst-value,0,0,0;
		HeatingDeltaTemperature.With(vec).Set(toFloat(m["diff-temp,K,inst-value,0,0,0"]))
		// power,W,inst-value,0,0,0;
		HeatingPower.With(vec).Set(toFloat(m["power,W,inst-value,0,0,0"]))
		// power,W,max-value,0,0,0;
		HeatingPowerMax.With(vec).Set(toFloat(m["power,W,max-value,0,0,0"]))
		// volume-flow,m3/h,inst-value,0,0,0;
		HeatingVolumeFlow.With(vec).Set(toFloat(m["volume-flow,m3/h,inst-value,0,0,0"]))
		// volume-flow,m3/h,max-value,0,0,0;
		HeatingVolumeFlowMax.With(vec).Set(toFloat(m["volume-flow,m3/h,max-value,0,0,0"]))
		// error-flags-dev-spec,,inst-value,0,0,0;
		// datetime,,inst-value,0,0,0;
		// energy,Wh,inst-value,0,0,1;
		// energy manufacturer-specific-02,Wh,inst-value,0,0,1;
		// manufacturer-specific-ff-07,,inst-value,0,0,1;
		// manufacturer-specific-ff-08,,inst-value,0,0,1;
		// volume,m3,inst-value,0,0,1;
		// power,W,max-value,0,0,1;
		// volume-flow,m3/h,max-value,0,0,1;
		// date,,inst-value,0,0,1;
		// manufacturer-specific-ff-0f,,inst-value,0,0,0;
		// manufacturer-specific-ff-11,,inst-value,0,0,0;
		// manufacturer-specific-ff-12,,inst-value,0,0,0;
		// manufacturer-specific-ff-1a,,inst-value,0,0,0;
		// fw-version,,inst-value,0,0,0

	case "water":
		Water.With(vec).Set(toFloat(m["volume,m3,inst-value,0,0,0"]))
	default:
		return fmt.Errorf("unknown device type: %s", m["device-type"])
	}

	return nil
}

func toFloat(s string) float64 {
	s = strings.Replace(s, ",", ".", 1)

	f, err := strconv.ParseFloat(s, 64)
	if err != nil {
		return 0
	}

	return f
}

func removeNonAscii(s string) string {
	return strings.Map(func(r rune) rune {
		if r > unicode.MaxASCII {
			return -1
		}
		return r
	}, s)
}
